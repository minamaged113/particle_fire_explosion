#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <ctime>
#include <SDL.h>

#include "utils/utils.h"
#include "screen/Screen.h"


int main(int argc, char *argv[])
{
    bool bQuit = false; // Quit flag
    utils::Logger logger("pfe.log");

    logger << "Writing this to a file.";
    logger << "Hello, World!";

    screen::Screen app;
    if (!app.init())
    {
        logger << "Initialization Failed";
        return EXIT_FAILURE;
    }


    Uint32 color = 0x000000FF;
    
    while (app.processEvents())
    {// game loop
        std::stringstream ss;
        ss << "0x" << std::setw(8) << std::setfill('0') << std::hex << color;
        logger << ss.str();
        app.setColor(color);
        app.update();
        color+=0xFFF;
    }
    app.close();
    return EXIT_SUCCESS;
}