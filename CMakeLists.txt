cmake_minimum_required(VERSION 3.10)

set(TARGET "PFE_TWP") # Particle Fire Explosion - Training Wheels Protocol

project(
    ${TARGET}
    VERSION 1.0.0
    LANGUAGES CXX
)

set(SDL2_DIR "${CMAKE_SOURCE_DIR}/config/cmake/")
find_package(SDL2 REQUIRED)

include_directories(${SDL2_INCLUDE_DIRS})

message(STATUS "SDL2 LIBS: ${SDL2_LIBRARIES}")
message(STATUS "SDL2 HEADERS: ${SDL2_INCLUDE_DIRS}")

add_subdirectory(utils)
add_subdirectory(screen)

add_executable(
    ${TARGET}
    main.cpp
)

target_link_libraries(
    ${TARGET}
    PRIVATE
        ${SDL2_LIBRARIES}
        utils
        screen
)
