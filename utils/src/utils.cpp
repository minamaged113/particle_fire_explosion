#include "utils/utils.h"


utils::Logger::Logger()
{
}

utils::Logger::Logger(std::string filename)
    : sFileName(filename)
{
    fLogFile.open(sFileName);
}

void const utils::Logger::operator<<(std::string message)
{
    if (fLogFile.is_open())
    {
        fLogFile << message << std::endl;
    }
    else
    {
        std::cout << message << std::endl;
    }
}

utils::Logger::~Logger()
{
    if (fLogFile.is_open())
    {
        fLogFile.close();
    }   
}
