#if !defined(UTILS_H)
#define UTILS_H

#include <iostream>
#include <fstream>

namespace utils {
    class Logger {
        private:
            std::string sFileName;
            std::ofstream fLogFile;
            bool bMachineReadable = false;
        public:
            Logger();
            Logger(std::string filename);
            Logger(std::string filename, bool machine_readable);
            ~Logger();
            void const operator<<(std::string message);
    };
}

#endif // UTILS_H
