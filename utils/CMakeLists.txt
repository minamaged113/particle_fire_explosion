set(UTILS_LIB_TARGET "utils")
add_library(
    ${UTILS_LIB_TARGET}
    STATIC
        src/utils.cpp
)

target_include_directories(
    ${UTILS_LIB_TARGET}
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
)