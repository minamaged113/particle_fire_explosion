#include "screen/Screen.h"

screen::Screen::Screen(/* args */)
    : m_renderer(nullptr),
    m_texture(nullptr),
    m_window(nullptr)
{
    m_buffer = new Uint32[SCREEN_HEIGHT*SCREEN_WIDTH];
    memset(m_buffer, 0xFF, SCREEN_HEIGHT*SCREEN_WIDTH*sizeof(Uint32));
}

screen::Screen::~Screen()
{
    // delete buffer to free memory
    delete [] m_buffer;
}

bool screen::Screen::init()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        return false;
    }

    m_window = SDL_CreateWindow(
        "Particle Fire Explosion",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        SDL_WINDOW_SHOWN
    );

    if (!m_window)
    { // if window not initialized
        SDL_Quit();
        return false;
    }

    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_PRESENTVSYNC);
    if (!m_renderer)
    {
        SDL_DestroyWindow(m_window);
        SDL_Quit();
        return false;
    }

    m_texture = SDL_CreateTexture(
        m_renderer,
        SDL_PIXELFORMAT_RGBA8888,
        SDL_TEXTUREACCESS_STATIC,
        SCREEN_WIDTH,
        SCREEN_HEIGHT
    );
    if (!m_texture)
    {
        SDL_DestroyWindow(m_window);
        SDL_Quit();
        return false;
    }
    
    setColor(0x4C2B3600);
    update();

    return true;
}

void screen::Screen::close()
{
    // Probably the order of destruction matters
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyTexture(m_texture);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}

bool screen::Screen::processEvents()
{
    while(SDL_PollEvent(&m_event))
    {// check event for bQuit event
        if (m_event.type == SDL_QUIT)
        {
            return false;
        }
    }
    return true;
}

void screen::Screen::update()
{
    SDL_UpdateTexture(
        m_texture,
        NULL,
        m_buffer,
        SCREEN_WIDTH*sizeof(Uint32)
    );
    SDL_RenderClear(m_renderer);
    SDL_RenderCopy(m_renderer, m_texture, NULL, NULL);
    SDL_RenderPresent(m_renderer);
}

void screen::Screen::setPixel(
    unsigned int xPos,
    unsigned int yPos,
    Uint8 red,
    Uint8 green,
    Uint8 blue
)
{
    Uint32 color = 0;
    color += red;
    color <<= 8;
    color += green;
    color <<= 8;
    color += blue;
    color <<= 8;
    color += 0xFF;

    Uint32 pixelLocation = xPos + yPos * SCREEN_WIDTH;
    m_buffer[pixelLocation] = color;
}

void screen::Screen::setColorRgb(Uint32 R, Uint32 G, Uint32 B)
{
    for (Uint16 col=0; col<SCREEN_WIDTH; col++)
    {
        for (Uint16 row=0; row<SCREEN_HEIGHT; row++)
        {
            setPixel(col, row, R, G, B);
        }
    }
}

void screen::Screen::setColor(Uint32 color)
{
    Uint8 alpha, red, green, blue;

    alpha = 0xFF;
    blue = (color & 0x0000FF00) >> 8;
    green = (color & 0x00FF0000) >> 16;
    red = (color & 0xFF000000) >> 24;
    screen::Screen::setColorRgb(red, green, blue);
}