#ifndef SCREEN_H
#define SCREEN_H

#include <SDL.h>

namespace screen {
class Screen
{
private:
    SDL_Window *m_window;
    SDL_Renderer *m_renderer;
    SDL_Texture *m_texture;
    Uint32 *m_buffer;
    SDL_Event m_event;
public:
    const static int SCREEN_WIDTH = 800;
    const static int SCREEN_HEIGHT = 600;
    Screen(/* args */);
    ~Screen();

    /**
     * @brief Function used to initialize window, renderer, and
     * texture.
     * 
     * @return true if program succeeds to initialize SDL
     * @return false if program fails to initialize SDL
     */
    bool init();

    /**
     * @brief Check for SDL events
     * 
     * @return true When no event is captured
     * @return false When an even has been captured
     */
    bool processEvents();

    /**
     * @brief Will be used for deallocation of memory
     * 
     */
    void close();

    /**
     * @brief Set the Pixel color
     * 
     * @param xPos x value of the point
     * @param yPos y value of the point
     * @param red Value of red color. From 0 to 255.
     * @param green Value of green color. From 0 to 255.
     * @param blue Value of blue color. From 0 to 255.
     */
    void setPixel(
        unsigned int xPos,
        unsigned int yPos,
        Uint8 red,
        Uint8 green,
        Uint8 blue
    );

    /**
     * @brief Update screen with buffer.
     * 
     */
    void update();

    /**
     * @brief Set the Color of the screen
     * 
     * @param R Red color value
     * @param G Green color value
     * @param B Blue color value
     */
    void setColorRgb(Uint32 R, Uint32 G, Uint32 B);

    /**
     * @brief Set the Color of the screen
     * 
     * @param color Color in 4 bytes format. RGBA format. A value is ignored.
     */
    void setColor(Uint32 color);
};



} // namespace screen

#endif // SCREEN_H