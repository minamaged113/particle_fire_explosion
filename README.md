# Particle Fire Explosion - Training Wheels Protocol

## Prepare development environment

This uses SDL2 version 2.0.20
### Using Choco

- Install choco: https://chocolatey.org/install
- using choco install the following:
  ```powershell
  choco install mingw --version 10.2.0 -y
  ```

### Prepare development environment with VSCode

- Download CMake Extensions
  - Add kits manually if needed to the following file: `C:\Users\<USERNAME>\AppData\Local\CMakeTools\cmake-tools-kits.json`
- This project has been tested with:
  - `MinGW GCC 10.2.0 x86_64-w64-mingw32`
- URL for LibSDL projects: https://www.libsdl.org/projects/
- The following have extracted into `<SDL2_DIR>`
- Download SDL_image and extract it into path without spaces
- Download SDL_mixer and extract it into path without spaces
- Download SDL2 and extract it into path without spaces
- The `sdl2-config.cmake` file is already copied from `<SDL2_DIR>/SDL2-2.0.20/x86_64-w64-mingw32/lib/cmake/SDL2` to `./config/cmake` in this project.
- ⚠ Change `prefix` variable in `./config/cmake/sdl2-config.cmake` with the path to your compiler directories.
- Add the include directory inside `./.vscode/c_cpp_properties.json` to search paths for correct intellisense functionality
  - It should look something like this: `<SDL2_DIR>/SDL2-2.0.20/x86_64-w64-mingw32/include/SDL2`

### Sample configure command

```cmd
cmake \
--no-warn-unused-cli \
-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE \
-DCMAKE_BUILD_TYPE:STRING=Debug \
-DCMAKE_C_COMPILER:FILEPATH=C:\ProgramData\chocolatey\bin\gcc.exe \
-DCMAKE_CXX_COMPILER:FILEPATH=C:\ProgramData\chocolatey\bin\g++.exe \
-Sc:/Users/<USERNAME>/playground/cpp/udemy-cpp-complete-beginners \
-Bc:/Users/<USERNAME>/playground/cpp/udemy-cpp-complete-beginners/build \
-G Ninja
```

### Sample build command

```cmd
cmake.exe \
--build c:/Users/<USERNAME>/playground/cpp/udemy-cpp-complete-beginners/build \
--config Debug \
--target all --
```